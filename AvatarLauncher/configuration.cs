﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuration
{
    public class c
    {
        // News and Content configuration
        // Remote directories
        public const int MAXARTICLES = 3;
        public const string rootUrl = "http://localhost/";
        public const string r_newsDir = "l_news/";
        public const string r_headerDir = r_newsDir + "_header/header";
        public const string r_contentDir = r_newsDir + "_content/content";

        public const string r_patchDir = "l_patches/";
        public const string r_patchFileName = "fileList.php";
        public const string r_patchFileSize = "fileSize.php";
        public const string r_versionDir = r_patchDir + "versionNew";

        // Sidelinks Configuration
        public const string r_Site = "";
        //public const string 

        // In-game related configuration
        // Local directories
        public const string l_cacheDir = "Cache";
        public const string l_currentVersion = "0.01";
        public const string l_patchDir = "Data/";

    }
}
