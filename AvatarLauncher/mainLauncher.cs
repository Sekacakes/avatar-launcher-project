﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Collections;
using System.Drawing.Text;
using System.Net.NetworkInformation;
using Configuration;

namespace AvatarLauncher
{
    public partial class MainLauncher : Form
    {
        // Event handling dependencies
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public List<string> fileList;
        public List<string> fileSizeList;

        // Defaults - Patcher
        public bool patchCompleted = false;
        public bool downloadCompleted;

        public long percByFile;

        public MainLauncher()
        {
            // Check if content server is online
            CheckServersAlive();

            // Load all components
            InitializeComponent();

            // Clears the client cache
            ClearCache(); 

            // Get News Articles
            LoadNewsContent("header");  // Parse headers
            LoadNewsContent("content"); // Parse content

            // Dynamic styling based on content
            DynamicNewsArticleStyling();

            // Display latest version
            GetCurrentVersion();

            // Begin patching
            LoadPatcher();
        }

        // News:
        #region News Handling

        private void LoadNewsContent(string type)
        {
            string typeUrl = "";

            for (int i = 1; i < c.MAXARTICLES + 1; ++i)
            {
                if (type == "header")
                    typeUrl = c.rootUrl + c.r_headerDir + i + ".php";
                else if (type == "content")
                    typeUrl = c.rootUrl + c.r_contentDir + i + ".php";

                var urlRequest = WebRequest.Create(@typeUrl);

                using (var response = urlRequest.GetResponse())
                using (var content = response.GetResponseStream())
                using (var reader = new StreamReader(content))
                {
                    var strContent = reader.ReadToEnd();
                    SetArticleContent(i, type, strContent);
                }
            }
        }

        private void SetArticleContent(int i, string type, string strContent)
        {
            switch (i)
            {
                case 1:
                    if (type == "header")
                        header_1.Text = strContent.ToString();
                    else if (type == "content")
                        content_1.Text = strContent.ToString();
                    break;
                case 2:
                    if (type == "header")
                        header_2.Text = strContent.ToString();
                    else if (type == "content")
                        content_2.Text = strContent.ToString();
                    break;
                case 3:
                    if (type == "header")
                        header_3.Text = strContent.ToString();
                    else if (type == "content")
                        content_3.Text = strContent.ToString();
                    break;
                default:
                    break;
            }
        }
        #endregion

        // Patcher:
        #region Patcher

        private void LoadPatcher()
        {
            // Method to patch client
            // 1. Get the list of files needed
            // 2. Cycle through each file in the list
            // 3. If theres a difference...
            // -----> Download missing files
            // -----> Update current files to match
            
            GetRemoteFiles();
            CheckPatchDiff(); // Method to compare local files against another
        }

        // Sub Function -> Parse Remote Files:
        #region Parse Files
        private void GetRemoteFiles()
        {
            // Get contents of files and filesizes (for checking)
            // Parsed data is split by commas (,)

            // Example --
            // 1.mpq,2.mpq,3.mpq,4.mpq
            // --->
            // fileList[0] = 1.mpq
            // fileList[1] = 2.mpq

            // fileSize[0] = 10 (bytes)
            // fileSize[1] = 20 (bytes)

            GetRemoteFileData("name");
            GetRemoteFileData("size");
        }

        private void GetRemoteFileData(string type)
        {
            string fileUrl = "";

            // Get all file names to be checked
            if (type == "name")
                fileUrl = c.rootUrl + c.r_patchDir + c.r_patchFileName;
            else if (type == "size")
                fileUrl = c.rootUrl + c.r_patchDir + c.r_patchFileSize;

            var fileListings = WebRequest.Create(@fileUrl);

            using (var response = fileListings.GetResponse())
            using (var content = response.GetResponseStream())
            using (var reader = new StreamReader(content))
            {
                var strContent = reader.ReadToEnd();

                string[] filesArray = strContent.Split(',');

                if (type == "name")
                {
                    fileList = new List<string>(filesArray.Length);
                    fileList.AddRange(filesArray);
                }
                else if (type =="size")
                {
                    fileSizeList = new List<string>(filesArray.Length);
                    fileSizeList.AddRange(filesArray);
                }

                reader.Dispose();
            }
        }
        #endregion

        // Sub Function -> Queue System:
        #region Queue/File Checking

        private void CheckDirectoryExists(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
                Directory.CreateDirectory(path);
        }

        private Queue<string> _queuedItems = new Queue<string>();

        private void CheckPatchDiff()
        {
            // Check for existing local patch directory
            CheckDirectoryExists(c.l_patchDir);

            UpdatePatchingStatus("checking", null);

            // Cycle through all files
            // Add them into the queue
            for (int i = 0; i < fileList.Count; ++i)
            {
                // Get the filename
                string filename = fileList[i];
                string localfilepath = c.l_patchDir + filename;

                // Find the local version of the file
                FileInfo localFile = new FileInfo(localfilepath);
                
                // Queue file if it doesn't exist in local files
                if (!localFile.Exists)
                    _queuedItems.Enqueue(filename);
                else
                {
                    // Check the sizes of the file, since the file exists already
                    long localLength = localFile.Length;

                    string filesize = fileSizeList[i];

                    if (localLength.ToString() != filesize)
                        _queuedItems.Enqueue(filename);
                }   
            }

            // No patch needed, up to date!
            if (!_queuedItems.Any())
            {
                patcherBar.Value = 100;
                percentageText.Text = patcherBar.Value.ToString() + "%";
            }

            // Process the queue
            ProcessQueue();
        }
        #endregion

        // Sub Function -> Queue Processing:
        #region Queue Processing -> Download

        private void ProcessQueue()
        {
            if (_queuedItems.Any())
            {
                WebClient client = new WebClient();
                client.DownloadProgressChanged += processQueue_DownloadProgressChanged;
                client.DownloadFileCompleted += processQueue_DownloadFileCompleted;

                var fileItem = _queuedItems.Dequeue();

                string remotefilepath = c.rootUrl + c.r_patchDir + fileItem;
                string localfilepath = c.l_patchDir + fileItem;

                UpdatePatchingStatus("updating", fileItem);
                client.DownloadFileAsync(new Uri(remotefilepath), localfilepath);
                return;
            }
            
            // End of download
            downloadCompleted = true;
            UpdatePatchingStatus("finished", null);

            if (patcherBar.Value == 100)
            {
                patchCompleted = true;
                this.playButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.ready;
                playButton.Refresh();

                this.patchDoneTag.BackgroundImage = global::AvatarLauncher.Properties.Resources.donetag;
                patchDoneTag.Refresh();
            }
        }

        private void processQueue_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            patcherBar.Value = int.Parse(Math.Truncate(percentage).ToString());
            percentageText.Text = Math.Truncate(percentage) + "%";
        }

        private void processQueue_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                // handle error scenario
                throw e.Error;
            }
            if (e.Cancelled)
            {
                // handle cancelled scenario
            }

            ProcessQueue();
        }

        private void UpdatePatchingStatus(string status, string fileName)
        {
            switch (status)
            {
                case "checking":
                    this.patchStatus.Text = "Checking newest patch data...";
                    break;
                case "updating":
                    this.patchStatus.Text = "Patching file: " + fileName + "...";
                    break;
                case "finished":
                    this.patchStatus.Text = "All files up-to-date...";
                    break;
            }
            patchStatus.Refresh();
        }

        #endregion

        #endregion

        // Misc: 
        // Clear Cache:
        // Get Latest Version:
        // Get Server Status:
        #region Misc Functions

        // Clear Cache
        private void ClearCache()
        {
            if (Directory.Exists(c.l_cacheDir))
                Directory.Delete(c.l_cacheDir);
        }

        private string GetCurrentVersion()
        {
            string versionUrl = c.rootUrl + c.r_versionDir + ".php";
            var versionRequest = WebRequest.Create(@versionUrl);

            using (var response = versionRequest.GetResponse())
            using (var content = response.GetResponseStream())
            using (var reader = new StreamReader(content))
            {
                var strContent = reader.ReadToEnd();
                versionNewest.Text = strContent.ToString(); // Display the newest version in launcher
                return strContent.ToString();
            }
        }

        // TO:DO -->
        private void GetServerStatus()
        {

        } // Get server status

        public static bool CheckHostAlive(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();

            try
            {
                PingReply reply = pinger.Send(nameOrAddress);

                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }

            return pingable;
        }

        private void CheckServersAlive()
        {
            try
            {
                var checkSiteAlive = WebRequest.Create(c.rootUrl);
                checkSiteAlive.GetResponse();
            }
            catch (System.Exception)
            {
                MessageBox.Show("Servers are currently offline. Please check the website for more information.");
                Close();
                System.Environment.Exit(1);
            }
        }

        private void VisitPage(string page)
        {
            System.Diagnostics.Process.Start(page);
        }

        #endregion

        // User Interface:

        #region Header Interaction

        private void _onMouseDown_Drag(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion

        #region Progress Bar
        public class ProgressBarEx : ProgressBar
        {
            private SolidBrush brush = null;

            public ProgressBarEx()
            {
                this.SetStyle(ControlStyles.UserPaint, true);
                SetStyle(ControlStyles.AllPaintingInWmPaint |
     ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                if (brush == null || brush.Color != this.ForeColor)
                    brush = new SolidBrush(this.ForeColor);

                Rectangle rec = new Rectangle(0, 0, this.Width, this.Height);
                if (ProgressBarRenderer.IsSupported)
                    ProgressBarRenderer.DrawHorizontalBar(e.Graphics, rec);

                rec.Width = (int)(rec.Width * ((double)Value / Maximum)) - 4;
                rec.Height = rec.Height - 4;
                e.Graphics.FillRectangle(brush, 2, 2, rec.Width, rec.Height);
            }
        }
        #endregion

        // Dynamic Styling
        #region Dynamic News Styling

        public class ScrollPanel : System.Windows.Forms.Panel
        {

            private const int WM_HSCROLL = 0x114;
            private const int WM_VSCROLL = 0x115;

            protected override void WndProc(ref Message m)
            {

                if ((m.Msg == WM_HSCROLL || m.Msg == WM_VSCROLL)
                && (((int)m.WParam & 0xFFFF) == 5))
                {
                    // Change SB_THUMBTRACK to SB_THUMBPOSITION
                    m.WParam = (IntPtr)(((int)m.WParam & ~0xFFFF) | 4);
                }
                base.WndProc(ref m);
            }
        }

        private void DynamicNewsArticleStyling()
        {
            // Globals
            int leftMarginInitial = 15;
            int heightMarginInitial = 2;

            int h1Size = 0;
            int c1Size = 0;
            int h2Size = 0;
            int c2Size = 0;

            // Set difference between each header title and actual content
            int headerContentMargin = 28;

            // Get height of all headers and articles content
            for (int i = 1; i < c.MAXARTICLES+1; ++i)
            {
                switch (i)
                {
                    case 1:
                        h1Size = GetArticleHeight(i, "header");
                        c1Size = GetArticleHeight(i, "content");
                        break;
                    case 2:
                        h2Size = GetArticleHeight(i, "header");
                        c2Size = GetArticleHeight(i, "content");
                        break;
                    }
            }

            // Position calculations of articles
            int marginHeader1 = heightMarginInitial;
            int marginContent1 = marginHeader1 + headerContentMargin;

            int marginHeader2 = h1Size + c1Size;
            int marginContent2 = marginHeader2 + headerContentMargin;

            int marginHeader3 = marginHeader2 + h2Size + c2Size;
            int marginContent3 = marginHeader3 + headerContentMargin;

            // Set positions

            this.header_1.Location = new System.Drawing.Point(leftMarginInitial, marginHeader1);
            this.content_1.Location = new System.Drawing.Point(leftMarginInitial, marginContent1);

            this.header_2.Location = new System.Drawing.Point(leftMarginInitial, marginHeader2);
            this.content_2.Location = new System.Drawing.Point(leftMarginInitial, marginContent2);

            this.header_3.Location = new System.Drawing.Point(leftMarginInitial, marginHeader3);
            this.content_3.Location = new System.Drawing.Point(leftMarginInitial, marginContent3);
        }

        private int GetArticleHeight(int i, string type)
        {
            switch (i)
            {
                case 1:
                    {
                        switch (type)
                        {
                            case "header":
                                return this.header_1.Size.Height;
                            case "content":
                                return this.content_1.Size.Height;
                        }
                    }
                    break;

                case 2:
                    {
                        switch (type)
                        {
                            case "header":
                                return this.header_2.Size.Height;
                            case "content":
                                return this.content_2.Size.Height;
                        }
                    }
                    break;
            }

            return 0;
        }
        #endregion

        // Buttons
        #region Minimize Button

        // Minimize button
        private void _minimize(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void _minimizeButton_MouseHover(object sender, EventArgs e)
        {
            this.minimizeButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.hoverminimize;
        }

        private void _minimizeButton_MouseHoverLeave(object sender, EventArgs e)
        {
            this.minimizeButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.normalminimize;
        }
        #endregion

        #region Exit Button

        // Exit button
        private void _exitButton_MouseHover(object sender, EventArgs e)
        {
            this.exitButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.pressedclose;
        }

        private void _exitButton_MouseHoverLeave(object sender, EventArgs e)
        {
            this.exitButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.normalclose;
        }

        private void _close(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            Environment.Exit(0);
        }
        #endregion

        #region Play Button

        // Play button
        private void _playButton_Click(object sender, EventArgs e)
        {
            if (patchCompleted == true)
            this.playButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.play_clicked;
        }

        private void _playButton_MouseHover(object sender, EventArgs e)
        {
            if (patchCompleted == true)
            this.playButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.play_clicked;
        }

        private void _playButton_MouseHoverLeave(object sender, EventArgs e)
        {
            if (patchCompleted == true)
            this.playButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.ready;
        }
        #endregion
        
        // Side Links
        #region Sign Up Link

        private void signUp_Click(object sender, EventArgs e)
        {
            VisitPage(c.rootUrl);
        }

        private void signUp_MouseHover(object sender, EventArgs e)
        {
            this.signUp.BackgroundImage = global::AvatarLauncher.Properties.Resources.signuphover;
        }

        private void signUp_MouseHoverLeave(object sender, EventArgs e)
        {
            this.signUp.BackgroundImage = global::AvatarLauncher.Properties.Resources.signupreg;
        }

        #endregion

        #region Forum Link

        private void forum_Click(object sender, EventArgs e)
        {
            VisitPage(c.rootUrl);
        }
        private void forum_MouseHover(object sender, EventArgs e)
        {
            this.forum.BackgroundImage = global::AvatarLauncher.Properties.Resources.forumhover;
        }

        private void forum_MouseHoverLeave(object sender, EventArgs e)
        {
            this.forum.BackgroundImage = global::AvatarLauncher.Properties.Resources.forumreg;
        }

        #endregion



    }
}
