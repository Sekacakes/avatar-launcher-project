﻿namespace AvatarLauncher
{
    partial class MainLauncher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.slideshow = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.versionPanel = new System.Windows.Forms.Panel();
            this.versionNewest = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.forum = new System.Windows.Forms.PictureBox();
            this.signUp = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.playButton = new System.Windows.Forms.PictureBox();
            this.newsPanel = new AvatarLauncher.MainLauncher.ScrollPanel();
            this.content_3 = new System.Windows.Forms.Label();
            this.header_3 = new System.Windows.Forms.Label();
            this.content_2 = new System.Windows.Forms.Label();
            this.header_2 = new System.Windows.Forms.Label();
            this.content_1 = new System.Windows.Forms.Label();
            this.header_1 = new System.Windows.Forms.Label();
            this.slideshowPanel = new System.Windows.Forms.Panel();
            this.patcherPanel = new System.Windows.Forms.Panel();
            this.patchStatus = new System.Windows.Forms.Label();
            this.patchDoneTag = new System.Windows.Forms.PictureBox();
            this.percentageText = new System.Windows.Forms.Label();
            this.patcherBar = new AvatarLauncher.MainLauncher.ProgressBarEx();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.minimizeButton = new System.Windows.Forms.PictureBox();
            this.exitButton = new System.Windows.Forms.PictureBox();
            this.headerPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.mainPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.slideshow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.versionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.forum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.signUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playButton)).BeginInit();
            this.newsPanel.SuspendLayout();
            this.patcherPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patchDoneTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            this.SuspendLayout();
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.MediumPurple;
            this.mainPanel.BackgroundImage = global::AvatarLauncher.Properties.Resources.main_frame;
            this.mainPanel.Controls.Add(this.panel1);
            this.mainPanel.Controls.Add(this.slideshow);
            this.mainPanel.Controls.Add(this.label5);
            this.mainPanel.Controls.Add(this.label4);
            this.mainPanel.Controls.Add(this.label3);
            this.mainPanel.Controls.Add(this.versionPanel);
            this.mainPanel.Controls.Add(this.forum);
            this.mainPanel.Controls.Add(this.signUp);
            this.mainPanel.Controls.Add(this.label1);
            this.mainPanel.Controls.Add(this.playButton);
            this.mainPanel.Controls.Add(this.newsPanel);
            this.mainPanel.Controls.Add(this.slideshowPanel);
            this.mainPanel.Controls.Add(this.patcherPanel);
            this.mainPanel.Controls.Add(this.minimizeButton);
            this.mainPanel.Controls.Add(this.exitButton);
            this.mainPanel.Controls.Add(this.headerPanel);
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(855, 593);
            this.mainPanel.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(452, 75);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(372, 194);
            this.panel1.TabIndex = 16;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::AvatarLauncher.Properties.Resources.Team_Avatar_fanart;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(-7, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(375, 270);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // slideshow
            // 
            this.slideshow.BackColor = System.Drawing.Color.Transparent;
            this.slideshow.Controls.Add(this.pictureBox3);
            this.slideshow.Location = new System.Drawing.Point(59, 79);
            this.slideshow.Name = "slideshow";
            this.slideshow.Size = new System.Drawing.Size(383, 128);
            this.slideshow.TabIndex = 15;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::AvatarLauncher.Properties.Resources.Sector_alto;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(4, -12);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(373, 182);
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(566, 381);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 32);
            this.label5.TabIndex = 14;
            this.label5.Text = "Server Status";
            this.label5.UseCompatibleTextRendering = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe Print", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(172, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 26);
            this.label4.TabIndex = 13;
            this.label4.Text = "News and Events";
            this.label4.UseCompatibleTextRendering = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(566, 275);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 32);
            this.label3.TabIndex = 6;
            this.label3.Text = "Client Patcher";
            this.label3.UseCompatibleTextRendering = true;
            // 
            // versionPanel
            // 
            this.versionPanel.BackColor = System.Drawing.Color.Transparent;
            this.versionPanel.Controls.Add(this.versionNewest);
            this.versionPanel.Controls.Add(this.label2);
            this.versionPanel.Location = new System.Drawing.Point(73, 533);
            this.versionPanel.Name = "versionPanel";
            this.versionPanel.Size = new System.Drawing.Size(333, 41);
            this.versionPanel.TabIndex = 12;
            // 
            // versionNewest
            // 
            this.versionNewest.AutoSize = true;
            this.versionNewest.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F);
            this.versionNewest.Location = new System.Drawing.Point(48, 13);
            this.versionNewest.Name = "versionNewest";
            this.versionNewest.Size = new System.Drawing.Size(31, 15);
            this.versionNewest.TabIndex = 3;
            this.versionNewest.Text = "0.01";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F);
            this.label2.Location = new System.Drawing.Point(14, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Patch:";
            // 
            // forum
            // 
            this.forum.BackColor = System.Drawing.Color.Transparent;
            this.forum.BackgroundImage = global::AvatarLauncher.Properties.Resources.forumreg;
            this.forum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.forum.Location = new System.Drawing.Point(6, 149);
            this.forum.Name = "forum";
            this.forum.Size = new System.Drawing.Size(53, 43);
            this.forum.TabIndex = 11;
            this.forum.TabStop = false;
            this.forum.Click += new System.EventHandler(this.forum_Click);
            this.forum.MouseLeave += new System.EventHandler(this.forum_MouseHoverLeave);
            this.forum.MouseHover += new System.EventHandler(this.forum_MouseHover);
            // 
            // signUp
            // 
            this.signUp.BackColor = System.Drawing.Color.Transparent;
            this.signUp.BackgroundImage = global::AvatarLauncher.Properties.Resources.signupreg;
            this.signUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.signUp.Location = new System.Drawing.Point(10, 52);
            this.signUp.Name = "signUp";
            this.signUp.Size = new System.Drawing.Size(43, 43);
            this.signUp.TabIndex = 5;
            this.signUp.TabStop = false;
            this.signUp.Click += new System.EventHandler(this.signUp_Click);
            this.signUp.MouseLeave += new System.EventHandler(this.signUp_MouseHoverLeave);
            this.signUp.MouseHover += new System.EventHandler(this.signUp_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F);
            this.label1.Location = new System.Drawing.Point(485, 438);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "The realms are offline...";
            // 
            // playButton
            // 
            this.playButton.BackColor = System.Drawing.Color.Transparent;
            this.playButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.not_ready;
            this.playButton.Location = new System.Drawing.Point(677, 405);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(140, 140);
            this.playButton.TabIndex = 9;
            this.playButton.TabStop = false;
            this.playButton.Click += new System.EventHandler(this._playButton_Click);
            this.playButton.MouseLeave += new System.EventHandler(this._playButton_MouseHoverLeave);
            this.playButton.MouseHover += new System.EventHandler(this._playButton_MouseHover);
            // 
            // newsPanel
            // 
            this.newsPanel.AutoScroll = true;
            this.newsPanel.BackColor = System.Drawing.Color.Transparent;
            this.newsPanel.Controls.Add(this.content_3);
            this.newsPanel.Controls.Add(this.header_3);
            this.newsPanel.Controls.Add(this.content_2);
            this.newsPanel.Controls.Add(this.header_2);
            this.newsPanel.Controls.Add(this.content_1);
            this.newsPanel.Controls.Add(this.header_1);
            this.newsPanel.Location = new System.Drawing.Point(64, 240);
            this.newsPanel.Name = "newsPanel";
            this.newsPanel.Size = new System.Drawing.Size(364, 282);
            this.newsPanel.TabIndex = 2;
            // 
            // content_3
            // 
            this.content_3.AutoSize = true;
            this.content_3.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F);
            this.content_3.Location = new System.Drawing.Point(20, 191);
            this.content_3.MaximumSize = new System.Drawing.Size(325, 0);
            this.content_3.Name = "content_3";
            this.content_3.Size = new System.Drawing.Size(70, 15);
            this.content_3.TabIndex = 5;
            this.content_3.Text = "CONTENT 3";
            // 
            // header_3
            // 
            this.header_3.AutoSize = true;
            this.header_3.Font = new System.Drawing.Font("Segoe Print", 11.25F);
            this.header_3.Location = new System.Drawing.Point(20, 167);
            this.header_3.Name = "header_3";
            this.header_3.Size = new System.Drawing.Size(96, 26);
            this.header_3.TabIndex = 4;
            this.header_3.Text = "HEADER 3";
            // 
            // content_2
            // 
            this.content_2.AutoSize = true;
            this.content_2.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F);
            this.content_2.Location = new System.Drawing.Point(16, 130);
            this.content_2.MaximumSize = new System.Drawing.Size(325, 0);
            this.content_2.Name = "content_2";
            this.content_2.Size = new System.Drawing.Size(70, 15);
            this.content_2.TabIndex = 3;
            this.content_2.Text = "CONTENT 2";
            // 
            // header_2
            // 
            this.header_2.AutoSize = true;
            this.header_2.Font = new System.Drawing.Font("Segoe Print", 11.25F);
            this.header_2.Location = new System.Drawing.Point(16, 108);
            this.header_2.Name = "header_2";
            this.header_2.Size = new System.Drawing.Size(96, 26);
            this.header_2.TabIndex = 2;
            this.header_2.Text = "HEADER 2";
            // 
            // content_1
            // 
            this.content_1.AutoSize = true;
            this.content_1.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.content_1.Location = new System.Drawing.Point(15, 30);
            this.content_1.MaximumSize = new System.Drawing.Size(325, 0);
            this.content_1.Name = "content_1";
            this.content_1.Size = new System.Drawing.Size(89, 15);
            this.content_1.TabIndex = 1;
            this.content_1.Text = "Content 1 Load";
            // 
            // header_1
            // 
            this.header_1.AutoSize = true;
            this.header_1.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.header_1.Location = new System.Drawing.Point(15, 2);
            this.header_1.MinimumSize = new System.Drawing.Size(325, 20);
            this.header_1.Name = "header_1";
            this.header_1.Size = new System.Drawing.Size(330, 32);
            this.header_1.TabIndex = 0;
            this.header_1.Text = "Lorem ipsum dolor sit amet, consectetur.";
            this.header_1.UseCompatibleTextRendering = true;
            // 
            // slideshowPanel
            // 
            this.slideshowPanel.BackColor = System.Drawing.Color.Transparent;
            this.slideshowPanel.Location = new System.Drawing.Point(878, 267);
            this.slideshowPanel.Name = "slideshowPanel";
            this.slideshowPanel.Size = new System.Drawing.Size(360, 129);
            this.slideshowPanel.TabIndex = 8;
            // 
            // patcherPanel
            // 
            this.patcherPanel.BackColor = System.Drawing.Color.Transparent;
            this.patcherPanel.Controls.Add(this.patchStatus);
            this.patcherPanel.Controls.Add(this.patchDoneTag);
            this.patcherPanel.Controls.Add(this.percentageText);
            this.patcherPanel.Controls.Add(this.patcherBar);
            this.patcherPanel.Controls.Add(this.pictureBox1);
            this.patcherPanel.Location = new System.Drawing.Point(452, 302);
            this.patcherPanel.Name = "patcherPanel";
            this.patcherPanel.Size = new System.Drawing.Size(372, 79);
            this.patcherPanel.TabIndex = 6;
            // 
            // patchStatus
            // 
            this.patchStatus.AutoSize = true;
            this.patchStatus.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F);
            this.patchStatus.Location = new System.Drawing.Point(15, 32);
            this.patchStatus.MinimumSize = new System.Drawing.Size(300, 0);
            this.patchStatus.Name = "patchStatus";
            this.patchStatus.Size = new System.Drawing.Size(300, 15);
            this.patchStatus.TabIndex = 9;
            this.patchStatus.Text = "Checking Patch Files...";
            this.patchStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // patchDoneTag
            // 
            this.patchDoneTag.BackgroundImage = global::AvatarLauncher.Properties.Resources.notdonetag;
            this.patchDoneTag.Location = new System.Drawing.Point(305, 7);
            this.patchDoneTag.Name = "patchDoneTag";
            this.patchDoneTag.Size = new System.Drawing.Size(53, 26);
            this.patchDoneTag.TabIndex = 8;
            this.patchDoneTag.TabStop = false;
            // 
            // percentageText
            // 
            this.percentageText.AutoSize = true;
            this.percentageText.Font = new System.Drawing.Font("Lucida Sans Unicode", 8.25F);
            this.percentageText.Location = new System.Drawing.Point(152, 56);
            this.percentageText.Name = "percentageText";
            this.percentageText.Size = new System.Drawing.Size(21, 15);
            this.percentageText.TabIndex = 6;
            this.percentageText.Text = "0%";
            // 
            // patcherBar
            // 
            this.patcherBar.BackColor = System.Drawing.Color.IndianRed;
            this.patcherBar.ForeColor = System.Drawing.Color.IndianRed;
            this.patcherBar.Location = new System.Drawing.Point(29, 13);
            this.patcherBar.Name = "patcherBar";
            this.patcherBar.Size = new System.Drawing.Size(260, 14);
            this.patcherBar.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::AvatarLauncher.Properties.Resources.progressbar;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(11, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(292, 23);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // minimizeButton
            // 
            this.minimizeButton.BackColor = System.Drawing.Color.Transparent;
            this.minimizeButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.normalminimize;
            this.minimizeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.minimizeButton.Location = new System.Drawing.Point(757, 3);
            this.minimizeButton.Name = "minimizeButton";
            this.minimizeButton.Size = new System.Drawing.Size(32, 24);
            this.minimizeButton.TabIndex = 1;
            this.minimizeButton.TabStop = false;
            this.minimizeButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this._minimize);
            this.minimizeButton.MouseLeave += new System.EventHandler(this._minimizeButton_MouseHoverLeave);
            this.minimizeButton.MouseHover += new System.EventHandler(this._minimizeButton_MouseHover);
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.BackgroundImage = global::AvatarLauncher.Properties.Resources.normalclose;
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.exitButton.Location = new System.Drawing.Point(786, 3);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(38, 24);
            this.exitButton.TabIndex = 0;
            this.exitButton.TabStop = false;
            this.exitButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this._close);
            this.exitButton.MouseLeave += new System.EventHandler(this._exitButton_MouseHoverLeave);
            this.exitButton.MouseHover += new System.EventHandler(this._exitButton_MouseHover);
            // 
            // headerPanel
            // 
            this.headerPanel.BackColor = System.Drawing.Color.Transparent;
            this.headerPanel.Location = new System.Drawing.Point(61, 27);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(782, 50);
            this.headerPanel.TabIndex = 0;
            this.headerPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this._onMouseDown_Drag);
            // 
            // MainLauncher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 593);
            this.Controls.Add(this.mainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainLauncher";
            this.Text = "AvatarEra: Alpha Launcher";
            this.TransparencyKey = System.Drawing.Color.MediumPurple;
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.slideshow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.versionPanel.ResumeLayout(false);
            this.versionPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.forum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.signUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playButton)).EndInit();
            this.newsPanel.ResumeLayout(false);
            this.newsPanel.PerformLayout();
            this.patcherPanel.ResumeLayout(false);
            this.patcherPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patchDoneTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.Panel headerPanel;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.PictureBox exitButton;
        private System.Windows.Forms.PictureBox minimizeButton;
        private ScrollPanel newsPanel;
        private System.Windows.Forms.Label header_1;
        private System.Windows.Forms.Label content_1;
        private System.Windows.Forms.Label content_3;
        private System.Windows.Forms.Label header_3;
        private System.Windows.Forms.Label content_2;
        private System.Windows.Forms.Label header_2;
        private System.Windows.Forms.Label versionNewest;
        private System.Windows.Forms.Panel patcherPanel;
        private ProgressBarEx patcherBar;
        private System.Windows.Forms.Panel slideshowPanel;
        private System.Windows.Forms.PictureBox playButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox signUp;
        private System.Windows.Forms.PictureBox forum;
        public System.Windows.Forms.Label percentageText;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox patchDoneTag;
        private System.Windows.Forms.Panel versionPanel;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Label patchStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel slideshow;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}

